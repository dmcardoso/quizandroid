package com.example.daniel.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

public class Perguntas extends Activity {


    List<Questao> questionList;
    int quid = 0;
    Questao questaoAtual;

    TextView txtQuestion;
    RadioButton rda, rdb, rdc;
    Button confirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perguntas);

        QuestaoDbHelper dbHelper = new QuestaoDbHelper(this);
        this.questionList = dbHelper.getAllQuestionsNotAnswereds();
        if (this.questionList.size() <= 0) {
            Intent i = new Intent(this, ResultadoActivity.class);
            startActivity(i);
        } else {
            Collections.shuffle(this.questionList);
            this.questaoAtual = this.questionList.get(this.quid);

            this.txtQuestion = (TextView) findViewById(R.id.large);
            this.rda = (RadioButton) findViewById(R.id.radio0);
            this.rdb = (RadioButton) findViewById(R.id.radio1);
            this.rdc = (RadioButton) findViewById(R.id.radio2);
            this.confirmar = (Button) findViewById(R.id.confirmar);
            if (this.questionList.size() > 0) {
                this.setQuestionView();
            }
        }

    }


    private void setQuestionView() {
        this.txtQuestion.setText(this.questaoAtual.getQuestao());
        this.rda.setText(this.questaoAtual.getOpca());
        this.rdb.setText(this.questaoAtual.getOpcb());
        this.rdc.setText(this.questaoAtual.getOpcc());
        this.quid++;
    }

    public void responder(View view) {
        RadioGroup grp = (RadioGroup) findViewById(R.id.radioGroup);
        RadioButton resposta = (RadioButton) findViewById(grp.getCheckedRadioButtonId());
        String textResposta = resposta.getText().toString();
        Resposta r = new Resposta();
        String respostaTxt = "";

        if (this.questaoAtual.getOpca().equals(textResposta)) {
            respostaTxt = "A";
        } else if (this.questaoAtual.getOpcb().equals(textResposta)) {
            respostaTxt = "B";
        } else if (this.questaoAtual.getOpcc().equals(textResposta)) {
            respostaTxt = "C";
        }

        if (respostaTxt.equals("A") || respostaTxt.equals("B") || respostaTxt.equals("C")) {
            QuestaoDbHelper rdb = new QuestaoDbHelper(this);
            r.setQuestao(this.questaoAtual.getId());
            if (respostaTxt.equals("A") && this.questaoAtual.getResposta().equals("A")) {
                Toast toast = Toast.makeText(this, "Acertou", Toast.LENGTH_LONG);
                toast.show();
                r.setResposta(respostaTxt);
                rdb.addAnswerToDB(r);
            } else if (respostaTxt.equals("B") && this.questaoAtual.getResposta().equals("B")) {
                Toast toast = Toast.makeText(this, "Acertou", Toast.LENGTH_LONG);
                toast.show();
                r.setResposta(respostaTxt);
                rdb.addAnswerToDB(r);
            } else if (respostaTxt.equals("C") && this.questaoAtual.getResposta().equals("C")) {
                Toast toast = Toast.makeText(this, "Acertou", Toast.LENGTH_LONG);
                toast.show();
                r.setResposta(respostaTxt);
                rdb.addAnswerToDB(r);
            } else {
                Toast toast = Toast.makeText(this, "Errou", Toast.LENGTH_LONG);
                toast.show();
                r.setResposta(respostaTxt);
                rdb.addAnswerToDB(r);
            }

            if (this.quid < this.questionList.size()) {
                resposta.setChecked(false);
                this.questaoAtual = this.questionList.get(this.quid);
                this.setQuestionView();
            } else {
                Intent i = new Intent(this, ResultadoActivity.class);
                startActivity(i);
            }
        }else{
            Toast toast = Toast.makeText(this, "Selecione uma alternativa", Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
