package com.example.daniel.quiz;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class EnviarEmail extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_email);
    }

    public void enviar(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("*/*");

        SQLiteOpenHelper questaodbhp = new QuestaoDbHelper(this);
        SQLiteDatabase db = questaodbhp.getReadableDatabase();
        String query = "SELECT  * from questoes";

        Cursor cursor = db.rawQuery(query, null);

        int total_perguntas = cursor.getCount();

        String query_acertos = "SELECT  * from questoes q inner join respostas r on q._id = r._id where q.resposta = r.resposta";
        Cursor cursor_acertos = db.rawQuery(query_acertos, null);


        int total_acertos = cursor_acertos.getCount();


        String texto_email = "Eu acertei " + total_acertos + " de " + total_perguntas + " perguntas !!!";

        EditText email = (EditText) findViewById(R.id.email);
        intent.putExtra(Intent.EXTRA_EMAIL, email.getText().toString());
        intent.putExtra(Intent.EXTRA_SUBJECT, "Progresso do quiz!");
        intent.putExtra(Intent.EXTRA_TEXT, texto_email);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
