package com.example.daniel.quiz;

public class Resposta {
    private Integer id, questao;
    private String resposta;

    public Resposta(int questao, String resposta) {
        this.questao = questao;
        this.resposta = resposta;
    }

    public Resposta(int id, int questao, String resposta) {
        this.id = id;
        this.questao = questao;
        this.resposta = resposta;
    }

    public Resposta() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestao() {
        return questao;
    }

    public void setQuestao(Integer questao) {
        this.questao = questao;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }
}
