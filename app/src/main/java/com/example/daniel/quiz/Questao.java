package com.example.daniel.quiz;

public class Questao {
    private Integer id;
    private String questao, opca, opcb, opcc, resposta;

    public Questao() {
    }

    public Questao(String questao, String opca, String opcb, String opcc, String resposta) {
        this.questao = questao;
        this.opca = opca;
        this.opcb = opcb;
        this.opcc = opcc;
        this.resposta = resposta;
    }

    public Questao(Integer id, String questao, String opca, String opcb, String opcc, String resposta) {
        this.id = id;
        this.questao = questao;
        this.opca = opca;
        this.opcb = opcb;
        this.opcc = opcc;
        this.resposta = resposta;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestao() {
        return questao;
    }

    public void setQuestao(String questao) {
        this.questao = questao;
    }

    public String getOpcb() {
        return opcb;
    }

    public void setOpcb(String opcb) {
        this.opcb = opcb;
    }

    public String getOpcc() {
        return opcc;
    }

    public void setOpcc(String opcc) {
        this.opcc = opcc;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public String getOpca() {

        return opca;
    }

    public void setOpca(String opca) {
        this.opca = opca;
    }
}
