package com.example.daniel.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void redefinirProgresso(View view) {
        QuestaoDbHelper rdb = new QuestaoDbHelper(this);

        rdb.redefinir();


        Toast toast = Toast.makeText(this, "Redefinido com sucesso!", Toast.LENGTH_LONG);
        toast.show();
    }

    public void listarPerguntas(View view) {
        Intent intent = new Intent(this, ListarPerguntasActivity.class);
        startActivity(intent);
    }

    public void cadastrarPerguntas(View view) {
        Intent intent = new Intent(this, AddQuestao.class);
        intent.putExtra(AddQuestao.EXTRA_IDQUESTAO, 0);
        startActivity(intent);
    }

    public void responderPerguntas(View view) {
        Intent i = new Intent(this, Perguntas.class);
        startActivity(i);
    }
}
