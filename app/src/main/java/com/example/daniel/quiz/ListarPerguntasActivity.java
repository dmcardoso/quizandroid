package com.example.daniel.quiz;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class ListarPerguntasActivity extends ListActivity {

    private SQLiteDatabase db;
    private Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListView listCursos = getListView();

        try {
            SQLiteOpenHelper questaohelper = new QuestaoDbHelper(this);
            this.db = questaohelper.getReadableDatabase();

            String selectQuery = "SELECT * FROM questoes";
            this.cursor = this.db.rawQuery(selectQuery, null);
            CursorAdapter listAdapter = new SimpleCursorAdapter(this,
                    android.R.layout.simple_list_item_1,
                    this.cursor,
                    new String[]{"questao"},
                    new int[]{android.R.id.text1},
                    0);

            listCursos.setAdapter(listAdapter);
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this, "DB indisponível", Toast.LENGTH_LONG);
            toast.show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }

    @Override
    public void onListItemClick(ListView listView, View itemView, int position, long id) {
        Intent i = new Intent(this, AddQuestao.class);
        i.putExtra(AddQuestao.EXTRA_IDQUESTAO, (int) id);
        startActivity(i);
    }
}
