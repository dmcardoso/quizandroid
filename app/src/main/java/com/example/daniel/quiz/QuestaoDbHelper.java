package com.example.daniel.quiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class QuestaoDbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "quizdb";
    private static final String DB_TABLE = "questoes";
    private static final String DB_TABLE_RESPOSTA = "respostas";

    private SQLiteDatabase dbase;
    private int rowCount = 0;

    public QuestaoDbHelper(Context context){
        super(context,DB_NAME,null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.dbase = db;
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
        db.execSQL("CREATE TABLE " + DB_TABLE + "('_id' INTEGER NOT NULL," +
                " 'questao' text NOT NULL," +
                " 'opca' TEXT NOT NULL," +
                " 'opcb' TEXT NOT NULL," +
                " 'opcc' TEXT NOT NULL," +
                " 'resposta' TEXT NOT NULL," +
                "  PRIMARY KEY ('_id'))");
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_RESPOSTA);
        db.execSQL("CREATE TABLE " + DB_TABLE_RESPOSTA + "('_id' INTEGER NOT NULL," +
                " 'questao' INTEGER NOT NULL," +
                " 'resposta' TEXT NOT NULL," +
                "  PRIMARY KEY ('_id'))");

        addQuestions();
//        db.close();
    }

    private void addQuestions() {
        Questao q1 = new Questao("Quanto é 2 + 2 ?", "2", "3", "4", "A");
        addQuestionToDB(q1);
        Questao q2 = new Questao("Quanto é 2 + 3 ?", "2", "3", "4", "B");
        addQuestionToDB(q2);
        Questao q3 = new Questao("Quanto é 2 + 4 ?", "2", "3", "4", "C");
        addQuestionToDB(q3);
        Questao q4 = new Questao("Quanto é 2 + 5 ?", "2", "3", "4", "B");
        addQuestionToDB(q4);
        Questao q5 = new Questao("Quanto é 2 + 6 ?", "2", "3", "4", "A");
        addQuestionToDB(q5);
    }

    public void addQuestionToDB(Questao q){
        ContentValues values = new ContentValues();
        values.put("questao", q.getQuestao());
        values.put("resposta",q.getResposta());
        values.put("opca",q.getOpca());
        values.put("opcb",q.getOpcb());
        values.put("opcc",q.getOpcc());
        this.dbase.insert(DB_TABLE, null, values);
    }

    public List <Questao> getAllQuestions(){
        List <Questao> questionList = new ArrayList<Questao>();

        this.dbase = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+ DB_TABLE;
        Cursor cursor = this.dbase.rawQuery(selectQuery,null);
        this.rowCount = cursor.getCount();

        if(cursor.moveToFirst()){
            do{
                Questao q = new Questao();
                q.setId(cursor.getInt(0));
                q.setQuestao(cursor.getString(1));
                q.setOpca(cursor.getString(2));
                q.setOpcb(cursor.getString(3));
                q.setOpcc(cursor.getString(4));
                q.setResposta(cursor.getString(5));

                questionList.add(q);

            }while (cursor.moveToNext());
        }
//        this.dbase.close();
        return questionList;
    }

    public void addAnswerToDB(Resposta r) {
        this.dbase = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put("questao", r.getQuestao());
        values.put("resposta", r.getResposta());
        this.dbase.insert(DB_TABLE_RESPOSTA, null, values);
    }

    public List<Resposta> getAllAnswers() {
        List<Resposta> answerList = new ArrayList<Resposta>();

        this.dbase = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + DB_TABLE_RESPOSTA;
        Cursor cursor = this.dbase.rawQuery(selectQuery, null);
        this.rowCount = cursor.getCount();

        if (cursor.moveToFirst()) {
            do {
                Resposta r = new Resposta();
                r.setId(cursor.getInt(0));
                r.setQuestao(cursor.getInt(1));
                r.setResposta(cursor.getString(2));

                answerList.add(r);

            } while (cursor.moveToNext());
        }
//        this.dbase.close();
        return answerList;
    }

    public void redefinir() {
        this.dbase = this.getReadableDatabase();
        this.dbase.execSQL(" DELETE FROM respostas WHERE _id > 0 ");
    }


    public List <Questao> getAllQuestionsNotAnswereds(){
        List <Questao> questionList = new ArrayList<Questao>();

        this.dbase = this.getReadableDatabase();
        String selectQuery = "SELECT q._id, q.questao, opca, opcb, opcc, q.resposta FROM questoes q left outer join respostas r on q._id = r._id where r.resposta is null";
        Cursor cursor = this.dbase.rawQuery(selectQuery,null);
        this.rowCount = cursor.getCount();

        if(cursor.moveToFirst()){
            do{
                Questao q = new Questao();
                q.setId(cursor.getInt(0));
                q.setQuestao(cursor.getString(1));
                q.setOpca(cursor.getString(2));
                q.setOpcb(cursor.getString(3));
                q.setOpcc(cursor.getString(4));
                q.setResposta(cursor.getString(5));

                questionList.add(q);

            }while (cursor.moveToNext());
        }
//        this.dbase.close();
        return questionList;
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+DB_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_RESPOSTA);
        onCreate(db);
    }

    public int getRowCount(){
        return this.rowCount;
    }
}
