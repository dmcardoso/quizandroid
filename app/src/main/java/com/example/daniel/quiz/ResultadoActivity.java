package com.example.daniel.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ResultadoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
    }

    public void enviarProgresso(View view) {
        Intent intent = new Intent(this, EnviarEmail.class);
        startActivity(intent);
    }

    public void redefinir(View view) {
        QuestaoDbHelper rdb = new QuestaoDbHelper(this);

        rdb.redefinir();


        Toast toast = Toast.makeText(this, "Redefinido com sucesso!", Toast.LENGTH_LONG);
        toast.show();

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void cadastrarPerguntas(View view) {
        Intent intent = new Intent(this, AddQuestao.class);
        intent.putExtra(AddQuestao.EXTRA_IDQUESTAO, 0);
        startActivity(intent);
    }
}
