package com.example.daniel.quiz;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddQuestao extends Activity {

    public static final String EXTRA_IDQUESTAO = "idquestao";

    private EditText questao, opca, opcb, opcc, resposta;
    private Questao q = new Questao();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_questao);

        this.questao = (EditText) findViewById(R.id.questao);
        this.opca = (EditText) findViewById(R.id.opca);
        this.opcb = (EditText) findViewById(R.id.opcb);
        this.opcc = (EditText) findViewById(R.id.opcc);
        this.resposta = (EditText) findViewById(R.id.resposta);

        int idquestao = (Integer) getIntent().getExtras().get(EXTRA_IDQUESTAO);
        if (idquestao > 0) {
            try {
                SQLiteOpenHelper questaodbhp = new QuestaoDbHelper(this);
                SQLiteDatabase db = questaodbhp.getReadableDatabase();
                String query = "SELECT  * FROM questoes where _id = " + idquestao;

                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {
                    this.q.setId(cursor.getInt(0));
                    this.q.setQuestao(cursor.getString(1));
                    this.q.setOpca(cursor.getString(2));
                    this.q.setOpcb(cursor.getString(3));
                    this.q.setOpcc(cursor.getString(4));
                    this.q.setResposta(cursor.getString(5));
                }
                cursor.close();
                db.close();

                this.questao.setText(this.q.getQuestao());
                this.opca.setText(this.q.getOpca());
                this.opcb.setText(this.q.getOpcb());
                this.opcc.setText(this.q.getOpcc());
                this.resposta.setText(this.q.getResposta());

            } catch (SQLiteException e) {
                Toast toast = Toast.makeText(this, "DB indispoível", Toast.LENGTH_LONG);
                toast.show();
            }
        }


    }

    public void salvar(View view) {
        String questao = "", opca = "", opcb = "", opcc = "", resposta = "";
        if (this.q.getId() != null && this.q.getId() > 0) {
            if (this.questao.getText().toString().trim().equals("")) {
                questao = this.q.getQuestao();
            } else {
                questao = this.questao.getText().toString().trim();
            }
            if (this.opca.getText().toString().trim().equals("")) {
                opca = this.q.getOpca();
            } else {
                opca = this.opca.getText().toString().trim();
            }
            if (this.opcb.getText().toString().trim().equals("")) {
                opcb = this.q.getOpcb();
            } else {
                opcb = this.opcb.getText().toString().trim();
            }
            if (this.opcc.getText().toString().trim().equals("")) {
                opcc = this.q.getOpcc();
            } else {
                opcc = this.opcc.getText().toString().trim();
            }
            if (this.resposta.getText().toString().trim().equals("")) {
                resposta = this.q.getResposta();
            } else {
                resposta = this.resposta.getText().toString().trim();
            }
            QuestaoDbHelper rdb = new QuestaoDbHelper(this);
            SQLiteDatabase db = rdb.getReadableDatabase();
            ContentValues v = new ContentValues();
            v.put("questao", questao);
            v.put("opca", opca);
            v.put("opcb", opcb);
            v.put("opcc", opcc);
            v.put("resposta", resposta);
            int id = this.q.getId();
            db.update("questoes", v, "_id = " + id, null);
            db.close();
            Toast toast = Toast.makeText(this, "Questão atualizada", Toast.LENGTH_LONG);
            toast.show();

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        } else {
            questao = this.questao.getText().toString().trim();
            opca = this.opca.getText().toString().trim();
            opcb = this.opcb.getText().toString().trim();
            opcc = this.opcc.getText().toString().trim();
            resposta = this.resposta.getText().toString().trim();
            if (!questao.equals("") && !opca.equals("") && !opcb.equals("") && !opcc.equals("") && !resposta.equals("")) {

                QuestaoDbHelper rdb = new QuestaoDbHelper(this);
                SQLiteDatabase db = rdb.getReadableDatabase();
                ContentValues v = new ContentValues();
                v.put("questao", questao);
                v.put("opca", opca);
                v.put("opcb", opcb);
                v.put("opcc", opcc);
                v.put("resposta", resposta);
                db.insert("questoes", "", v);

                db.close();
                Toast toast = Toast.makeText(this, "Questão cadastrada!", Toast.LENGTH_LONG);
                toast.show();

                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
            } else {
                Toast toast = Toast.makeText(this, "Todos os dados devem ser informados", Toast.LENGTH_LONG);
                toast.show();
            }

        }
    }
}
